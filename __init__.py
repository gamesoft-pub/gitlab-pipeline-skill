
import logging
import requests
import time
import yaml
import json 

from opsdroid.matchers import match_regex
from opsdroid.matchers import match_crontab
from opsdroid.skill import Skill
from opsdroid.events import Message

_LOGGER = logging.getLogger(__name__)

class GitlabHelper(Skill):
    jobSettings = {}
    userIDGroup = {}
    def __init__(self, opsdroid, config):
        self.jobSettings = config.get('jobConf', None)
        self.userIDGroup = config.get('userIDGroup', None)
        super().__init__(opsdroid, config)

    #指令詳細的說明
    @match_regex(r"/help_([^_]+)$")
    async def helpDetail(self, message):
        """/help_CMDTYPE - show CMD Help"""
        cmdData = await self.opsdroid.memory.get(message.user)
        response = []
        cmdType = message.regex.group(1)
        if await self.checkErrUserPermission(cmdType, message, True):
            return
        #response.append("cmdData:{}".format(cmdData))
        if cmdData != None and cmdData['cmdType'] != 'triggerStart':
            response.append("Hello, {}".format(message.user))
            response.append("你有未完成的指令暫存資訊, 如果要重新啟動請先清除")
            response.append("/reset")
        else:
            cmdData = {
                'cmdType' : 'triggerStart'
            }
            response = response + self.jobSettings[cmdType]['desc']
            await self.opsdroid.memory.put(message.user, cmdData)
        await message.respond("\n".join(response))

    #觸發/選擇 問答事件
    @match_regex(r"/help$")
    async def help(self, message):
        """/help - List /trigger commands"""
        cmdData = await self.opsdroid.memory.get(message.user)
        response = []
        #response.append("cmdData:{}".format(cmdData))
        if cmdData != None and cmdData['cmdType'] != 'triggerStart':
            response.append("Hello, {}".format(message.user))
            response.append("你有未完成的指令暫存資訊, 如果要重新啟動請先清除")
            response.append("/reset")
        else:
            cmdData = {
                'cmdType' : 'triggerStart'
            }
            response.append("Hello, {}".format(message.user))
            for cmdType in self.jobSettings:
                if not await self.checkErrUserPermission(cmdType, message, False):
                    response = response + self.jobSettings[cmdType]['shortDesc']
            await self.opsdroid.memory.put(message.user, cmdData)
        await message.respond("\n".join(response))

    #直接key好對應的參數輸入會走這個, 就不需要頻繁的來回
    @match_regex(r"/trigger@(\S+) ?(\S+)?$")
    async def triggerByFullCmd(self, message):
        response = []
        cmdType = message.regex.group(1)
        paramStr = message.regex.group(2)

        if await self.checkErrUserPermission(cmdType, message, True):
            return

        if 'JobArgs' not in self.jobSettings[cmdType]:
            await self.getArgsYaml()

        if paramStr == None:
            paramPairs = []
        else:
            paramPairs = paramStr.split(',')
            
        if len(paramPairs) != len(self.jobSettings[cmdType]['JobArgs']):
            response.append("Hello, {}, 參數數量好像有問題? 目前是{}個, 我需要{}個".format(message.user, len(paramPairs), len(self.jobSettings[cmdType]['JobArgs'])))
            
            response = response + self.jobSettings[cmdType]['desc']
            await message.respond("\n".join(response))
            return

        inputData = {}

        for paramPairStr in paramPairs:
            kvpair = paramPairStr.split(':')
            if kvpair[0] not in self.jobSettings[cmdType]['JobArgs']:
                response.append("Hello, {}, 參數好像輸入錯了, {} 並不在所需參數中 ?".format(message.user, kvpair[0]))
                response = response + self.jobSettings[cmdType]['desc']
                await message.respond("\n".join(response))
                return

            if kvpair[1] not in self.jobSettings[cmdType]['JobArgs'][kvpair[0]]:
                #檢查是否有允許自行輸入的, 會是 #開頭
                checkFail = True
                for rightReplyValue in self.jobSettings[cmdType]['JobArgs'][kvpair[0]]:
                    if rightReplyValue[0] == '#':
                        checkFail = False
                        break
                if checkFail:
                    response.append("Hello, {}, 參數好像輸入錯了, {} -> {} 並不在合法參數回應中, 請確認一下!".format(message.user, kvpair[0], kvpair[1]))
                    await message.respond("\n".join(response))
                    return

            inputData[kvpair[0]] = kvpair[1]

        formData = await self.genPipelineVariables(cmdType, inputData)
        shortCmd = await self.getShortCmdStr(cmdType, inputData)
        
        await self.curlGitlabPipeline(cmdType, formData, message, shortCmd)
        response.append("pipeline 已啟動, 後續會定期更新執行狀態")
        await message.respond("\n".join(response))
    

    #透過問答組成需要參數的版本
    @match_regex(r"/trigger_([^_]+)_?(\S+)?$")
    async def triggerAskParam(self, message):
        response = []
        cmdData = await self.opsdroid.memory.get(message.user)
        cmdType = message.regex.group(1)
        paramStr = message.regex.group(2) # 可能是None
        _LOGGER.debug(_("cmd start: %s"), cmdType)
        if await self.checkErrUserPermission(cmdType, message, True):
            _LOGGER.debug(_("no permission leave!"))
            return
        if 'JobArgs' not in self.jobSettings[cmdType]:
            await self.getArgsYaml()
        #response.append("paramStr: {}".format(paramStr))
        # 檢查暫存資料是否符合目前輸入的類型, 如果沒有暫存資料可能是直接打整串, 幫它初始化
        if cmdData == None:
            cmdData = {
                'cmdType' : 'triggerStart'
            }
        else:
            if cmdData['cmdType'] != cmdType and cmdData['cmdType'] != 'triggerStart':
                response.append("Hello, {}".format(message.user))
                response.append("你有未完成的指令暫存資訊, 如果要重新啟動請先清除")
                response.append("/reset")
                await message.respond("\n".join(response))
                return
        
        # 問答部份
        cmdData['cmdType'] = cmdType

        if 'replys' not in cmdData:
            cmdData['replys'] = {}
        # 有回應先更新回應資料
        if paramStr != None:
            params = paramStr.split('|')
            for param in params:
                kvpair = param.split('_', 1)
                cmdData['replys'][kvpair[0]] = kvpair[1]

        await self.opsdroid.memory.put(message.user, cmdData)

        # 檢查有沒有需要再詢問
        reply_markup = ''
        if len(cmdData['replys']) < len( self.jobSettings[cmdType]['JobArgs'] ):
            for replyKey in self.jobSettings[cmdType]['JobArgs']:
                if replyKey not in cmdData['replys']:
                    response.append("請選擇部屬需要的參數 {}:".format(replyKey))
                    reply_markup = await self.getMarkupString(cmdType, replyKey)
                    break

        else: # 需要的參數都回應了
            # 移除暫存資料
            await self.opsdroid.memory.delete(message.user)
            
            # 替換完畢後call gitlab pipeline
            formData = await self.genPipelineVariables(cmdType, cmdData['replys'])
            shortCmd = await self.getShortCmdStr(cmdType, cmdData['replys'])

            await self.curlGitlabPipeline(cmdType, formData, message, shortCmd)
            response.append("已啟動, 完成會再行通知, 或者可以從連結去看執行狀態")
            response.append("下面是這個指令的完整版, 可以複製下來後續貼上直接觸發...")
            response.append("{}".format(shortCmd))

        if reply_markup != '':
            await self.opsdroid.default_connector.send_btn_message(
                Message(
                            text="\n".join(response),
                            user=message.user,
                            user_id=message.user_id,
                            target=message.target,
                            connector=self.opsdroid.default_connector,
                ),
                reply_markup
            )
        else:
            await message.respond("\n".join(response))

    @match_regex(r"/reset$")
    async def reset(self, message):
        """/reset - Clear Old trigger temp data"""
        response = []
        await self.opsdroid.memory.delete(message.user)
        response.append("暫存資訊清除完畢")
        response.append("如果要重新觸發請按")
        response.append("/help")
        await message.respond("\n".join(response))

    # @match_regex(r"/echo$")
    # async def echoo(self, message):
    #     response = []
    #     response.append("123123")
    #     reply_markup='{"keyboard":[["/option1"],["/option2"]]}'
    #     reply_markup = json.dumps({"keyboard":[["/option1"],["/option2"]]}) 
        
    #     _LOGGER.debug(_("reply_markup: %s."), reply_markup)
    #     await self.opsdroid.default_connector.send_btn_message(
    #         Message(
    #                     text="\n".join(response),
    #                     user=message.user,
    #                     user_id=message.user_id,
    #                     target=message.target,
    #                     connector=self.opsdroid.default_connector,
    #         ),
    #         reply_markup
    #     )



######################################################
#
#           以下是tool function
#
######################################################


    # 抓取gitlab job的參數定義回來
    async def getArgsYaml(self):
        for cmdType in self.jobSettings:
            if 'argsRawUrl' in self.jobSettings[cmdType]:
                headers = {'PRIVATE-TOKEN': self.jobSettings[cmdType]['statusToken']}
                res = requests.get(self.jobSettings[cmdType]['argsRawUrl'], headers = headers)
                data = yaml.load(res.content)
            else:
                data = {}
            self.jobSettings[cmdType]['JobArgs'] = data
        
        _LOGGER.debug(_("jobSetting: %s."), self.jobSettings)
        return

    # 檢查user是否符合使用者權限
    async def checkErrUserPermission(self, cmdType, message, showMsg):
        _LOGGER.debug(_("checkErrUserPermission: %s %s."), cmdType, showMsg)
        checkFail = True
        for group in self.jobSettings[cmdType]['userGroup']:
            if message.user_id in self.userIDGroup[group]:
                checkFail = False
                break
        if checkFail and showMsg:
            response = []
            response.append("Hello, {}".format(message.user))
            response.append("你目前無法使用此指令, 如有需求請找IT人員申請")
            await message.respond("\n".join(response))
        return checkFail

    # 整理variable 並 觸發gitlab pipeline
    async def genPipelineVariables(self, cmdType, data):
        form = {
            'token': (None, self.jobSettings[cmdType]['triggerJobToken']),
            'ref': (None, 'master'),
        }
        for replyKey in data:
            form["variables[{}]".format(replyKey)] = data[replyKey]
        for extraKey in self.jobSettings[cmdType]['extraParamAppend']:
            if self.jobSettings[cmdType]['extraParamAppend'][extraKey][0:13] == '__copy_from__':
                form["variables[{}]".format(extraKey)] = data[self.jobSettings[cmdType]['extraParamAppend'][extraKey][13:]]
            else:
                form["variables[{}]".format(extraKey)] = self.jobSettings[cmdType]['extraParamAppend'][extraKey]
        
        _LOGGER.debug(_("form: %s."), form)
        return form

    # 觸發gitlab pipeline
    async def curlGitlabPipeline(self, cmdType, formData, message, shortCmd):
        response = []
        res = requests.post(self.jobSettings[cmdType]['triggerUrl'], formData)
        jsonData = res.json()
        _LOGGER.debug(_("jsonData: %s."), jsonData)
        response.append("pipelineID: {}".format(jsonData['id']))
        response.append("status: {}".format(jsonData['status']))
        response.append("web_url: {}".format(jsonData['web_url']))
        await message.respond("\n".join(response))
        await self.addRunningPipeline(cmdType, jsonData['id'], message, shortCmd)
        return

    # 加入pipeline待檢查列表
    async def addRunningPipeline(self, cmdType, pipelineID, message, shortCmd):
        sysData = await self.opsdroid.memory.get("gitlabHelper")
        if sysData == None:
            sysData = {}
        if 'runningPipelines' not in sysData:
            sysData['runningPipelines'] = []

        sysData['runningPipelines'].append({
            'cmdType' : cmdType, 
            'pipelineID' : pipelineID, 
            'user' : message.user,
            'user_id' : message.user_id,
            'target' : message.target,
            'triggerBy' : shortCmd,
        })
        await self.opsdroid.memory.put('gitlabHelper', sysData)
    
    # 定期檢查 是不是有運作中的pipeline並且通知使用者
    @match_crontab('* * * * *', timezone="Asia/Taipei")
    async def checkPipelineStatus(self, event):
        sysData = await self.opsdroid.memory.get("gitlabHelper")
        if sysData == None or 'runningPipelines' not in sysData:
            return

        for idx, runningPipeline in reversed(list(enumerate(sysData['runningPipelines']))):
            # runningPipeline -> [ data, pipelineid, message ]
            cmdType = runningPipeline['cmdType']
            headers = {'PRIVATE-TOKEN': self.jobSettings[cmdType]['statusToken']}
            url = "{}{}".format(self.jobSettings[cmdType]['statusUrl'], runningPipeline['pipelineID'])
            res = requests.get(url, headers = headers)
            jsonData = res.json()
            if jsonData['status'] == 'success' or jsonData['status'] == 'failed':
                response = []
                response.append("pipelineID: {}".format(jsonData['id']))
                response.append("triggerBy: {}".format(runningPipeline['triggerBy']))
                response.append("status: {}".format(jsonData['status']))
                response.append("started_at: {}".format(jsonData['started_at']))
                response.append("finished_at: {}".format(jsonData['finished_at']))

                await self.opsdroid.send(
                    Message(
                        text="\n".join(response),
                        user=runningPipeline['user'],
                        user_id=runningPipeline['user_id'],
                        target=runningPipeline['target'],
                        connector=self.opsdroid.default_connector
                    )
                )
                sysData['runningPipelines'].pop(idx)
        sysData = await self.opsdroid.memory.put("gitlabHelper",sysData)

    async def getShortCmdStr(self, cmdType, kvData):
        cmdStr = "/trigger@{} ".format(cmdType)
        for replyKey in kvData:
            cmdStr = "{}{}:{},".format(cmdStr, replyKey, kvData[replyKey])
        cmdStr = cmdStr[0:-1]
        return cmdStr
    
    async def getMarkupString(self, cmdType, replyKey):
        reply_markup = {
            "keyboard" : [],
            "resize_keyboard" : True,
            "one_time_keyboard" : True
        }
        btnRow = []
        for idx, option in list(enumerate(self.jobSettings[cmdType]['JobArgs'][replyKey])):
            # if idx % 2 == 0:
            #     btnRow = [
            #         "/trigger_{}_{}_{}".format(cmdType, replyKey, option),
            #     ]
            # else:
            #     btnRow.append("/trigger_{}_{}_{}".format(cmdType, replyKey, option))
            #     reply_markup["keyboard"].append(btnRow)
            #     btnRow = []
            reply_markup["keyboard"].append(["/trigger_{}_{}_{}".format(cmdType, replyKey, option)])
        # if len(btnRow) != 0:
        #     reply_markup["keyboard"].append(btnRow)

        _LOGGER.debug(_("reply_markup: %s."), json.dumps(reply_markup))
        return json.dumps(reply_markup)