# gitlab-pipeline-skill

# 設定方式

``` yaml
skills:
  gitlabhelper:
    repo: https://gitlab.com/gamesoft-pub/gitlab-pipeline-skill.git
    jobConf:
      命令類型(ex: "AAA_Pipeline"):
        desc: 
          - '======================================='
          - '指令敘述'
          - '======================================='
        triggerJobToken: 'Gitlab API TOKEN, can trigger pipeline'
        triggerUrl: 'Gitlab pipeline trigger url, ex: https://gitlab.com/api/v4/projects/123456789/trigger/pipeline'
        statusToken: 'Gitlab Project Deploy Token, 可以取得pipeline狀態以及抓取project檔案'
        statusUrl: '取得pipeline狀況的api url, ex: https://gitlab.com/api/v4/projects/123456789/pipelines/'
        argsRawUrl: '取得機器人參數列表的檔案raw path, ex:https://gitlab.com/api/v4/projects/123456789/repository/files/JobArgs/raw?ref=master'
        extraParamAppend: # 觸發pipeline 額外帶上的參數, 可以加上 __copy_from__參數名, 指定額外參數複製特定參數的數值
          extraParm1: '123'
          extraParam2: '__copy_from__ReplyArg1'
        
```
